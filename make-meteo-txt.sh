spool_dir="/data/spool"
base_dir=$(realpath $(dirname $0))
${base_dir}/period-update.sh >${spool_dir}/meteo.json

. ${base_dir}/venv/bin/activate
python ${base_dir}/compile-meteo.py ${base_dir}/meteo.j2 ${spool_dir}/meteo.json >${spool_dir}/meteo.txt
