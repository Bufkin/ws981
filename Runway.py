import math
from WindWorks import WindWork
from collections import OrderedDict
LEFT = 'left'
RIGHT = 'right'
DIRECT = 'direct'
NOT_ALLOWED = 'not_allowed'

DIRECTION = [LEFT, RIGHT, NOT_ALLOWED]


class Runway(object):
    _angle = 0

    def __init__(self, angel):
        self._angle = angel
        self._cross_wind = None

    def is_allowed(self, wind_dir):
        dir_list = self.get_allowed_wind_range()
        for d in dir_list:
            if d[0] <= wind_dir <= d[1]:
                return True
        return False

    def get_allowed_wind_range(self):
        r = []
        if self._angle - 89 > 0 and self._angle + 90 < 360:
            r.append((self._angle - 89, self._angle + 90))
        else:
            if (self._angle - 89) < 0:
                r.append((360+(self._angle - 89), 360))
                r.append((0, self._angle))
            else:
                r.append((self._angle - 90, self._angle))

            if (self._angle + 90) > 360:
                r.append((self._angle, 0))
                r.append((0, (self._angle - 270)))
            else:
                r.append((self._angle, self._angle + 90))

        return r

    def get_wind_dir(self, wind_angle):
        if self.is_allowed(wind_angle):
            angle = wind_angle - self._angle

            radian_angle = math.radians(90 - angle)
            v = round(math.cos(radian_angle), 4)
            if v < 0:
                return LEFT
            if v > 0:
                return RIGHT
            return DIRECT
        else:
            return NOT_ALLOWED

    def crosswind(self, wind_angle, wind_speed):
        return math.fabs(WindWork.crosswind(self._angle, wind_speed, wind_angle))

    def get_context(self, wind_angle, wind_speed):
        response = {'is_allowed': self.is_allowed(wind_angle)}
        if response['is_allowed']:
            response['wind_from_side'] = self.get_wind_dir(wind_angle)
            response['cross_wind_kt'] = self.crosswind(
                wind_angle, WindWork.ms2tk(wind_speed))
            response['cross_wind_ms'] = self.crosswind(wind_angle, wind_speed)
            response['runway_angle'] = self._angle
            response['runway_id'] = "%02d" % int(self._angle / 10)
            response['runway_name'] = "RNW %s" % response['runway_id']
        return response


class RunwaySelector(object):
    _runways = []

    def __init__(self):
        self._runways = []

    def add(self, runway):
        self._runways.append(runway)

    def recommend(self, wind_dir, wind_speed):
        allowed = []
        for runway in self._runways:
            if runway.is_allowed(wind_dir):
                allowed.append(runway)

        windy = OrderedDict()
        for runway in allowed:
            cross_wind = runway.crosswind(wind_dir, wind_speed)
            windy[cross_wind] = runway

        return windy

    def get_context(self, wind_dir, wind_speed):
        runways = self.recommend(wind_dir, wind_speed)
        response = []
        for r in runways:
            response.append(runways[r].get_context(wind_dir, wind_speed))
        return response
