from Runway import RunwaySelector, Runway
import argparse
import json

RUNWAY_ANGLES = [23, 5]

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Select runway.')
    parser.add_argument('dir', metavar='dir', type=int,
                        nargs=1, help='wind direction')
    parser.add_argument('speed', metavar='speed',
                        type=float, nargs=1, help='wind speed')
    parser.add_argument('runway', type=argparse.FileType(
        'w'), default="data/src/runway_result", help="information about used runway")
    args = parser.parse_args()
    resp = []
    runways = RunwaySelector()
    for runway_angle in RUNWAY_ANGLES:
        runways.add(Runway(runway_angle * 10))
        dir = float(args.dir[0])
        speed = float(args.speed[0])
        d = runways.get_context(dir, speed)
        resp.append(d)
        # print(d)

    json.dump(d, args.runway, indent=4)
