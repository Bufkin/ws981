import argparse
import redis


def get_wind_speed_data():
    r = redis.Redis(host='localhost', port=6379, db=0)
    l = r.xrange('wind_actual_speed')
    return [float(b[1].get(b'wind_actual_speed', None).decode('ASCII')) for b in l if b[1].get(b'wind_actual_speed', None)]


def count_speed_data():
    d = get_wind_speed_data()

    avg_speed = sum(d) / len(d)
    max_speed = max(d)

    gust_title = "Gust" if (max_speed**1.9438444924574) - \
        (avg_speed**1.9438444924574) > 5 else "Max"

    return (avg_speed, max_speed, gust_title)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Count wind variable.')
    parser.add_argument('gustkt', type=argparse.FileType(
        'w'), default="data/src/wind_actual_gust_kt", help="file for wind gust KT")
    parser.add_argument('gustms', type=argparse.FileType(
        'w'), default="data/src/wind_actual_gust_ms", help="file for wind gust MS")
    parser.add_argument('gust_title', type=argparse.FileType(
        'w'), default="data/src/wind_actual_gust_title", help="file for wind gust title")
    parser.add_argument('windkt', type=argparse.FileType(
        'w'), default="data/src/wind_actual_speed_kt", help="file for avg speed in KT")
    parser.add_argument('windms', type=argparse.FileType(
        'w'), default="data/src/wind_actual_speed_ms", help="file for avg speed in MS")
    args = parser.parse_args()

    avg_speed, gust, gust_title = count_speed_data()

    args.windkt.write(str(avg_speed*1.9438444924574))
    args.windms.write(str(avg_speed))

    args.gust_title.write(str(gust_title))
    args.gustms.write(str(gust))
    args.gustkt.write(str(gust*1.9438444924574))
    # print(f"AVG speed {avg_speed}m/s, AVG speed {avg_speed}kt, {gust_title} {gust}m/s, {gust_title} {gust}kt")
