import argparse
import redis


def get_wind_dir_data():
    r = redis.Redis(host='localhost', port=6379, db=0)
    l = r.xrange('wind_actual_dir')
    return [float(b[1].get(b'wind_actual_dir', None).decode('ASCII')) for b in l if b[1].get(b'wind_actual_dir', None)]


def get_variable(dir):
    d = get_wind_dir_data()
    max_angel = dir + 90
    min_angel = dir - 90

    if max_angel > 360:
        max_angel_over360 = max_angel - 360
    else:
        max_angel_over360 = None

    if min_angel < 0:
        min_angel_over0 = 360 + min_angel
    else:
        min_angel_over0 = None

    max_lst = [dir, ]
    min_lst = [dir, ]

    for mad in d:
        if dir <= mad <= max_angel:
            max_lst.append(mad)
        elif max_angel_over360 is not None and mad < max_angel_over360:
            max_lst.append(360 + mad)
        elif min_angel < mad < dir:
            min_lst.append(mad)
        elif min_angel_over0 is not None and mad < min_angel_over0:
            min_lst.append(360 - mad)

    mx = max(max_lst)
    if mx > 360:
        mx -= 360
    mi = min(min_lst)
    return (mi, mx)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Count wind variable.')
    parser.add_argument('dir', metavar='dir', type=int,
                        nargs=1, help='wind direction')
    parser.add_argument('minfile', type=argparse.FileType(
        'w'), default="data/src/wind_variable_min", help="file name for min wind variable")
    parser.add_argument('maxfile', type=argparse.FileType(
        'w'), default="data/src/wind_variable_max", help="file name for max wind variable")
    args = parser.parse_args()
    dir = args.dir[0]
    mi, mx = get_variable(dir)

    args.minfile.write(str(mi))
    args.maxfile.write(str(mx))
    print(f"Min {mi}° - Current {dir}° - max {mx}°")
