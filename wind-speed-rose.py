import argparse
import redis
import json


def get_wind_speed_data():
    r = redis.Redis(host='localhost', port=6379, db=0)
    l = r.xrange('wind_actual_speed')
    return [float(b[1].get(b'wind_actual_speed', None).decode('ASCII')) for b in l if b[1].get(b'wind_actual_speed', None)]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Count wind variable.')
    parser.add_argument('wind_rose', type=argparse.FileType(
        'w'), default="data/src/wind_rose", help="data for wind rose")
    args = parser.parse_args()
    d = get_wind_speed_data()
    json.dump(d, args.wind_rose, indent=4)
